﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health_bar : MonoBehaviour {

    public GameObject personaggio;
    private Text testo;

	// Use this for initialization
	void Start () {
        testo = gameObject.GetComponent<Text>();

    }
	
	// Update is called once per frame
	public void UpdateVita () {
        if (personaggio.GetComponent<Health>().health >= 0)
        {
            testo.text = "Health: " + personaggio.GetComponent<Health>().health;
        }
        else
        {
            testo.text = "Health: 0";
        }
	}
}
