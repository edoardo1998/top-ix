﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

    public GameObject personaggio;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (personaggio.GetComponent<Health>().health > 0)
        {
            gameObject.transform.position = new Vector3(personaggio.transform.position.x, personaggio.transform.position.y+3, personaggio.transform.position.z - 20);
        }

	}
}
