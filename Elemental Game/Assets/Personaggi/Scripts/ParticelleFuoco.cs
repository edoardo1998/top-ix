﻿using UnityEngine;
using System.Collections;

public class ParticelleFuoco : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.parent.localScale.x > 0)
        {
            gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 170));
        }
        else
        {
            gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }
}
