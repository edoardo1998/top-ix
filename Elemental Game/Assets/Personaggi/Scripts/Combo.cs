﻿using UnityEngine;
using System.Collections;

public class Combo : MonoBehaviour
{
    public string nome;
    public GameObject personaggio;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ComboActivator(Collider2D coll)
    {
        
        GameObject babbo = coll.gameObject.transform.parent.gameObject;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            float dist;
            if ((coll.gameObject.transform.parent.gameObject.transform.GetChild(1).GetComponent<Combo>().nome.Equals("fireboy") && nome.Equals("earthboy")) || (coll.gameObject.transform.parent.gameObject.transform.GetChild(1).GetComponent<Combo>().nome.Equals("earthboy") && nome.Equals("fireboy")))
            {
                dist = coll.transform.position.x - transform.position.x;
                if (dist > 0 && (!ParticelleAbilitate(2, babbo) || ParticelleAbilitate(1, personaggio)))
                {
                    DisabilitaParticelle(1, babbo);
                    DisabilitaParticelle(1, personaggio);
                    AbilitaParticelle(2, babbo);
                    DisabilitaParticelle(2, personaggio);
                    DisabilitaLava(personaggio);
                    AbilitaLava(babbo);

                }
                else if (dist <= 0 && (!ParticelleAbilitate(2, personaggio) || ParticelleAbilitate(1, personaggio)))
                {
                    DisabilitaParticelle(1, babbo);
                    DisabilitaParticelle(1, personaggio);
                    AbilitaParticelle(2, personaggio);
                    DisabilitaParticelle(2, babbo);
                    AbilitaLava(personaggio);
                    DisabilitaLava(babbo);
                }
            }
            else if ((coll.gameObject.transform.parent.gameObject.transform.GetChild(1).GetComponent<Combo>().nome.Equals("fireboy") && nome.Equals("cloudboy")) || (coll.gameObject.transform.parent.gameObject.transform.GetChild(1).GetComponent<Combo>().nome.Equals("cloudboy") && nome.Equals("fireboy")))
            {
                dist = coll.transform.position.x - transform.position.x;
                if (dist > 0 && (!ParticelleAbilitate(2, babbo) || ParticelleAbilitate(1, personaggio)))
                {
                    DisabilitaParticelle(1, babbo);
                    DisabilitaParticelle(1, personaggio);
                    AbilitaParticelle(3, babbo);
                    DisabilitaParticelle(3, personaggio);
                    if (babbo.transform.localScale.x > 0)
                    {
                        babbo.transform.GetChild(4).localRotation = Quaternion.Euler(new Vector3(0,0,170));
                    }
                    else
                    {
                        babbo.transform.GetChild(4).localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    }

                }
                else if (dist <= 0 && (!ParticelleAbilitate(2, personaggio) || ParticelleAbilitate(1, personaggio)))
                {
                    DisabilitaParticelle(1, babbo);
                    DisabilitaParticelle(1, personaggio);
                    AbilitaParticelle(3, personaggio);
                    DisabilitaParticelle(3, babbo);
                    if (gameObject.transform.localScale.x > 0)
                    {
                        gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 170));
                    }
                    else
                    {
                        gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    }
                }
            }
        }
}









    void DisabilitaParticelle (int i, GameObject g)
    {
        ParticleSystem.EmissionModule em = g.gameObject.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
        em.enabled = false;
    }

    void AbilitaParticelle (int i, GameObject g)
    {
        ParticleSystem.EmissionModule em = g.gameObject.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
    }

    bool ParticelleAbilitate(int i, GameObject g)
    {
        bool found = false;
        ParticleSystem.EmissionModule em = g.gameObject.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
        if (em.enabled==true)
        {
            found = true;
        }
        return found;
    }

    void AbilitaLava(GameObject g)
    {
        g.GetComponent<Ranged_Attack_Personaggi>().enabled = true;
    }
    void DisabilitaLava(GameObject g)
    {
        g.GetComponent<Ranged_Attack_Personaggi>().enabled = false;
    }
}