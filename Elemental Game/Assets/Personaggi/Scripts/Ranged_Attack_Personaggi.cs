﻿using UnityEngine;
using System.Collections;

public class Ranged_Attack_Personaggi : MonoBehaviour {

    private float cd=0;
    public float cooldown;
    public GameObject bulletPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            gameObject.GetComponent<Ranged_Attack_Personaggi>().enabled = false;
        }
        if (Input.GetKeyDown("e"))
        {
            if (Time.timeSinceLevelLoad - cd > cooldown)
            {
                FireBullet();
                cd = Time.timeSinceLevelLoad;
            }
        }
    }

    public void FireBullet()
    {

        //Clone of the bullet
        GameObject Clone;

        
            //spawning the bullet at position
            Clone = (Instantiate(bulletPrefab, transform.position, transform.rotation)) as GameObject;

            //add force to the spawned objected

            if (gameObject.transform.localScale.x < 0)
            {
                Clone.gameObject.transform.localScale = new Vector3(-Clone.gameObject.transform.localScale.x, Clone.gameObject.transform.localScale.y, 1);
                Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(300, 0));
            }
            else
            {

                Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300, 0));
            }
    }
}
