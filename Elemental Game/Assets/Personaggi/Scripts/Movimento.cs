﻿using UnityEngine;
using System.Collections;

// #m = soluzione dimmerda

public class Movimento : MonoBehaviour {

    
	Vector2 scala;
	public GameObject terra;
	public Vector3 posizione;
    public float gravità;
    public string nome;
	private float cd;
    private float cd_knock;
	private Rigidbody2D body;
    public bool knockbacked=false;
	private float hspeed,  vspeed; //horizontal and vertical speed
	public float Speed = 4; //max movement speed
	public float jump_speed = 8; //jump speed
	public bool grounded; //0 if grounded
	private const float ray_length = 0.05f;
	int verso = 1;

	void Start () {
		//starter speeds
		hspeed = 0;
		vspeed = 0;
		scala = new Vector2 (transform.localScale.x, transform.localScale.y);
		grounded = false;
		body = gameObject.GetComponent<Rigidbody2D> ();

	}

	void Update () {
		posizione = transform.position;

        IsGrounded();

        if (!knockbacked){
            if (nome == "cloudboy")
            {
                gameObject.GetComponent<Planata>().enabled = true;
            }
            
            if (verso == 1)
            {
                gameObject.transform.localScale = new Vector3(scala.x, scala.y, 1);
            }
            else
            {
                
                gameObject.transform.localScale = new Vector3(-scala.x, scala.y, 1);

            }
            if (Input.GetKey("a") || Input.GetKey("left"))
            {
                hspeed = -Speed;
                verso = -1;
            }
            else if (Input.GetKey("d") || Input.GetKey("right"))
            {
                hspeed = Speed;
                verso = 1;
            }
            else
            {
                hspeed = 0;
            }
            if ((Input.GetKeyDown("space")) && (grounded))
            {
                if (Time.timeSinceLevelLoad - cd > 1)
                {
                    body.AddForce(new Vector2(0, 500));
                    cd = Time.timeSinceLevelLoad;
                }
            }

            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(hspeed, vspeed + gameObject.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        else if(knockbacked && Time.timeSinceLevelLoad - cd_knock > 0.2 && grounded)
        {
            knockbacked = false;
        }
    }

	bool IsGrounded(){

		int layer_mask = 1 << 9;

		Vector3 position = terra.transform.position; //posizione sensore (sotto il pg)
		grounded = Physics2D.Raycast(position, Vector3.down, ray_length, layer_mask); //controllo terreno e memorizzo informazione
		Debug.DrawRay (position, Vector3.down * ray_length);
		return grounded;
	}

    public void IsKnockbacked()
    {
        gameObject.GetComponent<Rigidbody2D>().gravityScale = gravità;
        knockbacked = true;
        cd_knock= Time.timeSinceLevelLoad;
    }

    public bool getKnock()
    {
        return knockbacked;
    }
}