﻿using UnityEngine;
using System.Collections;

public class Combo_Controller : MonoBehaviour
{

    public GameObject personaggio;

    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            ParticleSystem.EmissionModule em;
            for (int i = 2; i < personaggio.transform.childCount-1; i++)
            {
                em = personaggio.gameObject.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
                if (em.enabled== true)
                {
                    em.enabled = false;
                }
            }
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (LayerMask.LayerToName(coll.gameObject.layer) == "combo")
            {
                personaggio.transform.GetChild(1).gameObject.GetComponent<Combo>().ComboActivator(coll);

            }
        }
    }
}
