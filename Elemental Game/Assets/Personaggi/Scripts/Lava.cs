﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

    public bool grounded = false;
    public GameObject bulletPrefab;
    public GameObject terra;
    private const float ray_length = 0.05f;
    private float cd = 0;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        GameObject Clone;
        IsGrounded();
        if (grounded)
        {
            for (int i = 0; i < 40; i++)
            {
                Clone = (Instantiate(bulletPrefab, transform.position, transform.rotation)) as GameObject;
                if (i < 16)
                {
                    Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 0));
                    
                }
                else if (i<31)
                {
                    Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 0));
                }
                else
                {
                    Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
                }
                Clone.SendMessage("setOrigine" , gameObject.transform.position.x);
            }
            
            Destroy(gameObject);
        }
	}

    bool IsGrounded()
    {

        int layer_mask = 1 << 9;

        Vector3 position = terra.transform.position; //posizione sensore (sotto il pg)
        grounded = Physics2D.Raycast(position, Vector3.down, ray_length, layer_mask); //controllo terreno e memorizzo informazione
        Debug.DrawRay(position, Vector3.down * ray_length);
        return grounded;
    }

    
}
