﻿using UnityEngine;
using System.Collections;

public class DannoPersonaggi : MonoBehaviour {
    
    public int danno;
    public int potenza_knock;

	// Use this for initialization
	void Start () {
	
	}
	
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (LayerMask.LayerToName(coll.gameObject.layer).Equals("nemici"))
        {
            if (coll.transform.position.x > gameObject.transform.position.x)
            {
                coll.gameObject.GetComponent<Artificial_Intelligence>().IsKnockbacked();
                Vector2 knock = new Vector2 (potenza_knock, potenza_knock);
                coll.GetComponent<Health_monelli>().Damage(danno, knock);
            }
            else
            {
                coll.gameObject.GetComponent<Artificial_Intelligence>().IsKnockbacked();
                Vector2 knock = new Vector2(-potenza_knock, potenza_knock);
                coll.GetComponent<Health_monelli>().Damage(danno, knock);
            }
        }
    }
}
