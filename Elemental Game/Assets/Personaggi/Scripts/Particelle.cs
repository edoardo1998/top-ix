﻿using UnityEngine;
using System.Collections;

public class Particelle : MonoBehaviour {

    // Use this for initialization
    ParticleSystem.EmissionModule em;

    void Start () {
        ParticleSystem.EmissionModule emiss = gameObject.GetComponent<ParticleSystem>().emission;
        emiss.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            em = gameObject.GetComponent<ParticleSystem>().emission;
            em.enabled= true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            em = gameObject.GetComponent<ParticleSystem>().emission;
            em.enabled = false; ;
        }

    }
}
