﻿using UnityEngine;
using System.Collections;

public class LavaLiquida : MonoBehaviour {

    private float cd;
    private float origine;
    float dist;

    // Use this for initialization
    void Start () {
        cd = Time.timeSinceLevelLoad;
        
	}
	
	// Update is called once per frame
	void Update () {
	
        dist= origine-transform.position.x;
        if (Time.timeSinceLevelLoad - cd >= 5 || (dist > 3 || dist<-3))
        {
            Destroy(gameObject);
        }
	}

    public void setOrigine(float posizione)
    {
        origine = posizione;
    }
}
