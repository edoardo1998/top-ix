﻿ using UnityEngine;
using System.Collections;



public class Planata : MonoBehaviour {

	bool IsGrounded;
	Movimento mov;
	float grav;

	void Start () {

		mov = gameObject.GetComponent<Movimento> ();

	}
	void Update () {

		IsGrounded = mov.grounded;

        if (!gameObject.GetComponent<Movimento>().getKnock())
        {
            if ((Input.GetKeyDown("space")) && (!IsGrounded) && !gameObject.GetComponent<Movimento>().knockbacked)
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x, 0);
                gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
            }

            if (Input.GetKeyUp("space") )
            {
                gameObject.GetComponent<Rigidbody2D>().gravityScale = gameObject.GetComponent<Movimento>().gravità;
            }
        }
        else
        {
            gameObject.GetComponent<Rigidbody2D>().gravityScale = gameObject.GetComponent<Movimento>().gravità;
        }
	}
}
