﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour {

    public int health = 100;
    public GameObject health_bar;
    float bar_damage;
    float health_bar_size;
    public GameObject text;
    private float cd_danno=0;
    private float cd_rosso = 0;
    private bool rosso = false;
    
    void Start()
    {
        health_bar_size = health_bar.transform.localScale.x;
    }
    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            
            Destroy(gameObject);
        }
        if (health > 100)
        {
            health = 100;
        }
        if (rosso && Time.timeSinceLevelLoad-cd_rosso > 0.5)
        {
            SpriteRenderer renderer = GetComponent<SpriteRenderer>();
            renderer.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    public void Damage(int danno, Vector2 knock_omg_power_yolo)
    {
        if (Time.timeSinceLevelLoad - cd_danno > 1)
        {
            bar_damage = health_bar_size / 100 * danno;
            health = health - danno;
            health_bar.transform.localScale = new Vector3(health_bar.transform.localScale.x - bar_damage, health_bar.transform.localScale.y, health_bar.transform.localScale.z);
            text.GetComponent<Health_bar>().UpdateVita();
            rossore();
            gameObject.GetComponent<Rigidbody2D>().AddForce(knock_omg_power_yolo);
            if (health < 50 && health > 20)
            {
                health_bar.GetComponent<Image>().color = new Color(255, 220, 0);

            }
            if (health < 20)
            {
                health_bar.GetComponent<Image>().color = new Color(200, 0, 0);
            }
            cd_danno = Time.timeSinceLevelLoad;
        }
    }

    public void rossore()
    {
        rosso = true;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.color = new Color(0.6f, 0f, 0f, 1f);
        cd_rosso = Time.timeSinceLevelLoad;
    }
}
