using UnityEngine;
using System.Collections;

// #m = soluzione dimmerda


public class Artificial_Intelligence : MonoBehaviour
{

    Vector2 scala;
    public float gravità;
    GameObject nearest;
    GameObject cloudboy;
    GameObject earthboy;
    GameObject fireboy;
    Vector3 pos_personaggio;
    public GameObject terra;
    private float cd_knock;
    private Rigidbody2D body;
    private float cd;           //cooldown movimento fuori range
    private float hspeed, vspeed; //velocità orizzontale e verticale
    private int i_spost;    //indice dello spostamento  #m
    public float Speed = 4; // velocità massima del movimento
    public float jump_speed = 8; //velocità di salto
    public bool grounded; //variabile che vale 0 se il personaggio è a terra
    public const float ray_length = 0.1f;
    int direction = 1;
    private bool knockbacked = false;
    Movimento script_cloudboy;
    void Start()
    {
        //velocità iniziali
        cd = Time.timeSinceLevelLoad;
        hspeed = 0;
        vspeed = 0;
        i_spost = 0;
        scala = new Vector2(transform.localScale.x, transform.localScale.y);
        grounded = false;
        cloudboy = GameObject.Find("Cloudboy");
        earthboy = GameObject.Find("Earthboy");
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {

        pos_personaggio = piuVicino().GetComponent<Movimento>().posizione;

        IsGrounded();

    if (!knockbacked)
    {
        float dist = Vector3.Distance(pos_personaggio, transform.position);
        if (dist > 2)
        {
            if (direction == 1)
            {
                gameObject.transform.localScale = new Vector3(scala.x, scala.y, 1);
            }
            else
            {
                gameObject.transform.localScale = new Vector3(-scala.x, scala.y, 1);
            }
            if (dist < 8)
            {
                i_spost = 0;
                if (pos_personaggio.x < transform.position.x)
                {
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x + (2 * Time.deltaTime), gameObject.transform.position.y);
                    hspeed = -Speed;
                    direction = 1;
                }
                else
                {
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x - (2 * Time.deltaTime), gameObject.transform.position.y);
                    hspeed = +Speed;
                    direction = -1;
                }


                if ((pos_personaggio.y > transform.position.y) && (IsGrounded()))
                {
                    if (Time.timeSinceLevelLoad - cd > 1)
                    {
                        body.AddForce(new Vector2(0, jump_speed * 100));
                        cd = Time.timeSinceLevelLoad;
                    }

                }
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(hspeed, vspeed + gameObject.GetComponent<Rigidbody2D>().velocity.y, 0);

            }
            else if (dist < 20 && dist > 8 && Time.timeSinceLevelLoad - cd > 3)
            {
                if (direction == -1 && i_spost < 90)
                {
                    if (i_spost == 90)
                    {
                        direction = -1;
                        i_spost = 0;
                        cd = Time.timeSinceLevelLoad;
                    }
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x + (2 * Time.deltaTime), gameObject.transform.position.y);
                    hspeed = -Speed;
                    i_spost++;


                }
                else
                {
                    if (i_spost == 90)
                    {
                        direction = 1;
                        i_spost = 0;
                        cd = Time.timeSinceLevelLoad;
                    }
                    else
                    {
                        gameObject.transform.position = new Vector3(gameObject.transform.position.x - (2 * Time.deltaTime), gameObject.transform.position.y);
                        hspeed = Speed;
                        i_spost++;
                    }
                }

            }
            else
            {
                hspeed = 0;
            }
        }
    }
    else if (knockbacked && Time.timeSinceLevelLoad - cd_knock > 0.2 && grounded)
    {
        knockbacked = false;
    }
}

    bool IsGrounded()
    {

        int layer_mask = 1 << 9;

        Vector3 position = terra.transform.position; //posizione sensore (sotto il pg)
        grounded = Physics2D.Raycast(position, Vector3.down, ray_length, layer_mask); //controllo terreno e memorizzo informazione
        Debug.DrawRay(position, Vector3.down * ray_length);
        return grounded;
    }

    public void IsKnockbacked()
    {
        gameObject.GetComponent<Rigidbody2D>().gravityScale = gravità;
        knockbacked = true;
        cd_knock = Time.timeSinceLevelLoad;
    }

    public bool getKnock()
    {
        return knockbacked;
    }

    GameObject piuVicino()
    {
        GameObject vicino;
        
        if (distanza(cloudboy) < distanza(earthboy))
        {
            vicino = cloudboy;
        }
        else
        {
            vicino = earthboy;
        }
        return vicino;
    }

    float distanza(GameObject g)
    {
            float dist = Vector3.Distance(g.transform.position, transform.position);
            return dist;
    }
}