﻿using UnityEngine;
using System.Collections;


public class Ranged_attack : MonoBehaviour
{

    //the object that will be spawned

    
    public GameObject bulletPrefab;
    private GameObject cloudboy;
    private GameObject earthboy;
    private Movimento script_personaggio;
    private Vector3 pos_personaggio;
    private float cd;
    private float dist;


    // Use this for initialization
    void Start()
    {
        cd = Time.realtimeSinceStartup;
        cloudboy = GameObject.Find("Cloudboy");
        earthboy = GameObject.Find("Earthboy");
    }

    // Update is called once per frame
    void Update()
    {

        pos_personaggio = piuVicino().GetComponent<Movimento>().posizione;

        dist = Vector3.Distance(pos_personaggio, transform.position);

        if (dist<8  && Time.timeSinceLevelLoad - cd > 0.8)
        {

            FireBullet();
            //look for and use the fire bullet operation
            cd = Time.timeSinceLevelLoad;
        }
    }




    public void FireBullet()
    {

        //Clone of the bullet
        GameObject Clone;

        //spawning the bullet at position
        Clone = (Instantiate(bulletPrefab, transform.position, transform.rotation)) as GameObject;

        //add force to the spawned objected

        if (pos_personaggio.x > transform.position.x)
        {
            Clone.gameObject.transform.localScale = new Vector3(-Clone.gameObject.transform.localScale.x, Clone.gameObject.transform.localScale.y, 1);
            Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(300,0));
        }
        else
        {
            
            Clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300, 0));
        }
    }


    GameObject piuVicino()
    {
        GameObject vicino;

        if (distanza(cloudboy) < distanza(earthboy))
        {
            vicino = cloudboy;
        }
        else
        {
            vicino = earthboy;
        }
        return vicino;
    }

    float distanza(GameObject g)
    {
        float distanza = Vector3.Distance(g.transform.position, transform.position);
        return distanza;
    }
}
