﻿using UnityEngine;
using System.Collections;

public class Remove : MonoBehaviour {

    float cd;
	// Use this for initialization
	void Start () {
        cd = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad - cd > 0.7)
        {
            Destroy(gameObject);
        }
	}
}
