﻿using UnityEngine;
using System.Collections;

    public class Danno : MonoBehaviour
    {
    
        public int danno;
        public int potenza_knock;
    
    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (LayerMask.LayerToName(coll.gameObject.layer).Equals("Personaggi"))
        {
            if (coll.transform.position.x > gameObject.transform.position.x)
            {
                coll.gameObject.GetComponent<Movimento>().IsKnockbacked();
                Vector2 knock = new Vector2(potenza_knock, potenza_knock);
                coll.GetComponent<Health>().Damage(danno, knock);
            }
            else
            {
                coll.gameObject.GetComponent<Movimento>().IsKnockbacked();
                Vector2 knock = new Vector2(-potenza_knock, potenza_knock);
                coll.GetComponent<Health>().Damage(danno, knock);
            }
        }
    }
}